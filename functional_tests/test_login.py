import re
import time

from django.core import mail
from guerrillamail import GuerrillaMailSession
from selenium.webdriver.common.keys import Keys

from .base import FunctionalTest

SUBJECT = "Your login link for Superlists"


class LoginTest(FunctionalTest):
    def wait_for_email(self, test_email, subject):
        if not self.staging_server:
            email = mail.outbox[0]
            self.assertIn(test_email, email.to)
            self.assertEqual(email.subject, subject)
            return email.body

        start = time.time()
        inbox = GuerrillaMailSession()
        try:
            inbox.set_email_address(test_email)
            while time.time() - start < 60:
                mail_list = inbox.get_email_list()
                if mail_list:
                    last_mail = mail_list[0]
                    if subject == last_mail.subject:
                        return inbox.get_email(last_mail.guid).excerpt
                time.sleep(5)
        finally:
            pass

    def test_can_get_email_link_to_log_in(self):
        # Edith goes to site
        # she notices a Log in section in the navbar for the first time
        # It's telling her to enter her email address, so she does
        if self.staging_server:
            test_email = 'edith.testuser@grr.la'
        else:
            test_email = 'edith@example.com'
        self.browser.get(self.live_server_url)
        self.browser.find_element_by_name("email").send_keys(test_email)
        self.browser.find_element_by_name("email").send_keys(Keys.ENTER)

        # A message appears telling her an email has been sent
        self.wait_for(
            lambda: self.assertIn(
                "Check your email", self.browser.find_element_by_tag_name("body").text
            )
        )

        # She checks her email and finds a message
        body  = self.wait_for_email(test_email, SUBJECT)

        # It has a url link in it
        self.assertIn("Use this link to log in", body)
        url_search = re.search(r"http://.+/.+$", body)
        if not url_search:
            self.fail(f"Could not find url in email body:\n{body}")
        url = url_search.group(0)
        self.assertIn(self.live_server_url, url)

        # she clicks it
        self.browser.get(url)

        # she is logged in!
        self.wait_to_be_logged_in(email=test_email)

        # Now she logs out
        self.browser.find_element_by_link_text("Log out").click()

        # She is logged out
        self.wait_to_be_logged_out(email=test_email)
